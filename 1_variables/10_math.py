#Python Arithmetic Operators
number_sum  = 1 + 2 
number_a = 3
print(f'Sum: {number_sum + number_a} ')

number_minus = 3 - 2
number_b = 1
print(f'Minus: {number_minus - number_b}')

number_mult = 2 * 2 
number_c = 2
print(f'Mult: {number_mult * number_c} ')

number_div = 10 / 2
number_d = 2
print(f'Div: {number_div / number_d} ')



#//: Divides the number on its left by the number on its right, rounds down the answer, and returns a whole number.
number_div_right = 5 // 2
print(f'Floor division: {number_div_right} ')

#Modulus
modulus_div = 5 % 2
print(f'Modulus div 5 % 2: {modulus_div}') 

#Exponentiation
number_exp = 2**2
print(f'Exp: {number_exp}')

import math
math.sqrt(81)
cases = 34 
cases_per = 34332
cases_result = cases_per / cases
print(cases_result)

#round 
print(f'{round(cases_result)}')
print(f'{round(cases_result, 5)}')