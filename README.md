# Data Structure with Python
<div align="center">
  <img height="300" width="200" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg"/>
  <img height="280" width="300" src="https://raw.githubusercontent.com/diegodila/design/main/icons/graphtree.svg"/>
</div>
  
- Big-O notation for analyzing algorithms

- Ordered and unordered vectors

- Stacks, Rows and Decks

- Single-threaded lists, double-ended linked lists, and double-threaded lists

- Recursion

- Sorting algorithms: bubble sort, selection sort, insertion sort, shell sort, merge sort and quick sort

- Binary search trees

- Graphs in Python

- Graph search algorithms, such as greedy search and A Estrela search (A*) in the field of Artificial Intelligence and the classic Dijkstra algorithm
